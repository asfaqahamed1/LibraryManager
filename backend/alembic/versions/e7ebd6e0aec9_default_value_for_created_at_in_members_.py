"""default value for created at in members table

Revision ID: e7ebd6e0aec9
Revises: ec5393afaf83
Create Date: 2023-09-30 14:03:08.671323

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'e7ebd6e0aec9'
down_revision: Union[str, None] = 'ec5393afaf83'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
