"""use server_default and server_onupdate as default key for bool as well

Revision ID: 48711fa697ac
Revises: c7955375c659
Create Date: 2023-10-16 22:32:32.479705

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '48711fa697ac'
down_revision: Union[str, None] = 'c7955375c659'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
