import requests
import os
from database.database_service import DataBaseService

class ImportBooksService():
    headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json'
    }
    def __init__(self, title=None, authors=None,publisher=None):
        self.params = {
            "title" : title,
            "authors": authors,
            "publisher": publisher
        }

    def import_books(self):
        """
        Import the books from the frappe api and update the datebase
        
        Keyword arguments: None
        Return: None
        """

        try:
            response = requests.request(url=os.environ.get("FRAPPE_LIBRARY_URL"), method="POST", params =self.params,headers=self.headers)
            return response.json()
        except Exception as e:    
            raise f"Importing Books failed with error {e}"

    def update_books(self,book_details):
        """
        Update the books table after importing the books from the frappe api
        
        Keyword arguments:
        bookDetails
        Return: None
        """
        try:
            for book_detail in book_details:
                DataBaseService().update_book_details(book_detail)
        except Exception as e:
            raise e

    def import_and_update_books(self):
        """
            Import and update the books to the database via frappe api
        """

        try:
            book_details = self.import_books()
            self.update_books(book_details.get('message', []))
        except Exception as e:
            raise e
        