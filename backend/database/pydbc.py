import os
from sqlalchemy import create_engine

class Pydbc(object):

    def get_connection(self):
        return create_engine(os.environ.get("DATABASE_URI"), echo=False)
        
    def __enter__(self):
         self.dbcon = self.get_connection()
         return self.dbcon
    
    def __exit__(self,exception_type, exception_value, traceback):
         with self.dbcon.connect() as connection:
              connection.close()