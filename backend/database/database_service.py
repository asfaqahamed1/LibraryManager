from database.dbconnection import DBconnection
from datetime import datetime

class DataBaseService(DBconnection):
    @classmethod
    def get_user(cls, name: str, password: str):
        """
        Args:
            name (str): user name for admin
            password (str): password
        """
        result = cls.execute(
            query=f"select user_name, password from admin where user_name='{name}' and password='{password}'"
        )
        return result

    @classmethod
    def update_book_details(cls, bookDetails):
        """
        Add or update the book fetched from the frappe api to the database
        """
        query = f'INSERT IGNORE INTO books \
                    (book_id,title,authors,average_rating,isbn,isbn13,language_code,num_page,ratings_count,text_reviews_count,publication_date,publisher,is_available) \
                        VALUES("{bookDetails.get("bookID")}","{bookDetails.get("title")}",\
                            "{bookDetails.get("authors")}", "{bookDetails.get("average_rating")}" ,\
                                "{bookDetails.get("isbn")}", "{bookDetails.get("isbn13")}",\
                                    "{bookDetails.get("language_code")}" ,"{bookDetails.get("num_pages")}" ,\
                                        "{bookDetails.get("ratings_count")}" ,"{bookDetails.get("text_reviews_count")}",\
                                            "{bookDetails.get("publication_date")}", "{bookDetails.get("publisher")}", {True});'
        try:
            result = cls.execute(query)
        except Exception as e:
            raise f"updation of books to database failed with error {e}"

    @classmethod
    def get_book_details(cls):
        """
        Get all the books available in the database

        Keyword arguments:
        argument -- None
        Returns: list
        """
        try:
            books = cls.execute('SELECT * FROM books')
            return books
        except Exception as e:
            raise f"Fetching the books failed with error {e}" 
    
    @classmethod
    def add_member(cls, member):
        try: 
            cls.execute(f"INSERT INTO members(name) VALUES('{member}');")
        except Exception as e:
            print(e)
            raise f"Adding member failed with error {e}"

    @classmethod
    def update_member(cls, member):
        try:
            cls.execute(f"UPDATE member SET name = {member['name']}, current_balance = {member['current_balance']} \
                WHERE member_id = {[member['member_id']]};")
        except Exception as e:
            print(e)
            raise f"Update member failed with error {e}"

    @classmethod
    def delete_one_row(cls, table, where_condition, where_column):
        try:
            cls.execute(f"DELETE FROM {table} where {where_column} = '{where_condition}';")
        except Exception as e:
            raise f"Deletion of row failed with error {e}"

    @classmethod
    def select_all(cls, table):
        try:
            table_data = cls.execute(f"select * from {table}")
            return table_data
        except Exception as e:
            raise f"Reading table failed with error {e}"

    @classmethod
    def insert_rent(cls, book_id,member_id):
        try:
            cls.execute(f"Insert into rental_details (member_id, book_id, is_returned,rented_at) VALUES('{member_id}', '{book_id}' , {False}, '{datetime.utcnow()}');")
        except Exception as e:
            raise f"Rent insertion failed with error {e}"
    
    @classmethod
    def close_rent(cls, rent_id, rent_fee):
        try:
            time = datetime.utcnow()
            cls.execute(f"Update rental_details set fee = '{rent_fee}', returned_at = '{time}', is_returned = {True} Where rental_details_id = '{rent_id}';")
        except Exception as e:
            raise f"Closing rent failed with error {e}"

    @classmethod
    def mark_book_status(cls, book_id, status):
        try:
            cls.execute(f"update books set is_available = '{status}' where book_id = '{book_id}'")
        except Exception as e:
            raise e 

    @classmethod
    def add_current_balance(cls, fee, member_id):
        try:
            cls.execute(f"update members set currrent_balance = current_balance + {fee} where member_id = '{member_id}'")
        except Exception as e:
            raise e


    @classmethod
    def update_book_detail(cls, book_id, title, authors, publisher):
        try:
            cls.execute(f"update books set title = '{title}', authors = '{authors}' , publisher = '{publisher}' where book_id = '{book_id}'")
        except Exception as e:
            raise e
