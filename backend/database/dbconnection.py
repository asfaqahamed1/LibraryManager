from database.pydbc import Pydbc
from sqlalchemy import text
from sqlalchemy.orm import Session
class DBconnection(object):
    connection = None
    def __init__(self) -> None:
        pass
    @classmethod
    def get_connection(cls, reconnect=False):
        if cls.connection ==None or reconnect:
            with Pydbc() as pydbc:
                cls.connection = pydbc
        return cls.connection
    
    @classmethod
    def execute(cls, query):
        try:
            result = cls.execute_qurey_with_connection(query)
        except Exception as error:
            cls.get_connection(reconnect=True)
            result = cls.execute_qurey_with_connection(query)
        return result
    
    @classmethod
    def execute_qurey_with_connection(cls,query):
        try:
            session = Session(bind=cls.connection)
            result = session.execute(text(query))
            result_set = []
            if result.returns_rows:
                if result.rowcount > 0:
                    result_set = result.mappings().all()
            session.commit()
            session.close()
            return result_set
        except Exception as e:
            raise e