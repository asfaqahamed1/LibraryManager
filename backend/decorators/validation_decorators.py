import functools
from flask import request, abort
from validators import login_validator, member_validators

def request_validator(validator_class):
    def decorator(api):
        @functools.wraps(api)
        def wrapper(*args, **kargws):
            payload = request.get_json()
            try:
                validator_class(**payload)
            except:
                abort(400, "Invalid request payload")
            return api(*args, **kargws)
        return wrapper
    return decorator