import os

class Config(object):
    SQLALCHEMY_DATABASE_URI=os.environ.get("DATABASE_URI")

class Production(Config):
    DEBUG=False

class Development(Config):
    DEBUG=True