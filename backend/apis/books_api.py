from flask import request, Blueprint, abort
from services.import_books_service import ImportBooksService
from database.database_service import DataBaseService
from helpers.utils import convert_row_mapping
from decorators.validation_decorators import request_validator
from validators.book_validator import UpdateBookValidator
import json


import_books_api = Blueprint('import_books_api', __name__)
@import_books_api.route('/import_books', methods=["GET"])
def import_books():
    """
    Api to fetch the book details and return all the books available in the database
    wrapper around the frappe api to search books based on authors, title, publishers.
    
    Params:
        title: str
        authors: str
        publishers: str

    Returns:
        book_details: list
    """
    title = request.args.get('title', None)
    authors= request.args.get('authors', None)
    publishers= request.args.get('publishers', None)
    print('authors', authors)
    if (title or authors or publishers):
        books_service = ImportBooksService(title,authors,publishers)
        books_service.import_and_update_books()
    books = DataBaseService().get_book_details()
    serializable_response = convert_row_mapping(books)
    return {"data": serializable_response}

delete_books_api = Blueprint('delete_book_api', __name__)
@delete_books_api.route('/delete_book',methods=["GET"])
def delete_book():
    """
        Api to delete book from records

        agrs:
            book_id: string
    """
    id = request.args.get('book_id')
    if id:
        try:
            DataBaseService().delete_one_row('books', id, 'book_id')
        except Exception as e:
            abort(500, "Internal server error")
        return json.dumps(True)


update_books_api = Blueprint('update_book_api', __name__)
@update_books_api.route('/update_book', methods=["POST"])
@request_validator(UpdateBookValidator)
def update_book():
    """
      Api to update the book and returns the updated book detailsupdateBook

      params:
        book_id: str
        title: str
        authors: str
        publisher: str

      returns:
        data: []
    """
    try: 
        book_details = request.get_json()
        DataBaseService().update_book_detail(book_details['book_id'], book_details['title'], book_details['authors'], book_details['publisher'])
        books = DataBaseService().get_book_details()
        serializable_response = convert_row_mapping(books)
        return {"data": serializable_response}
    except Exception as e:
        print(e)
        abort(500, 'Internal server error')
