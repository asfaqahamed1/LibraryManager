from flask import Blueprint,request, abort 
from helpers.utils import convert_row_mapping
from decorators.validation_decorators import request_validator
from validators.member_validators import AddMemberValidator, UpdateMemberValidator
from database.database_service import DataBaseService
import json

add_member_api = Blueprint('add_member_api', __name__)
@add_member_api.route('/addmember', methods=["POST"])
@request_validator(AddMemberValidator)
def add_member():
    """

    Api to add the member to the database

    sample payload: 
            {
                name: str
            }
    """
    payload = request.get_json()
    try:
        DataBaseService().add_member(payload['name'])
    except Exception as e:
        abort(500, "Internal server error")
    
    return json.dumps(True)

update_member_api = Blueprint('update_member_api', __name__)
@update_member_api.route('/updatemember', methods=["POST"])
@request_validator(UpdateMemberValidator)
def update_member():
    """
    Api to edit the member details

    sample payload:
    {
        memberId: int,
        name:str,
        current_balance: float
    }
    """
    payload = request.get_json()
    try:
        DataBaseService().update_member(payload)
    except Exception as e:
        abort(500, "Internal server error")
    return json.dumps(True)

delete_member_api = Blueprint('delete_member_api', __name__)
@delete_member_api.route('/deletemember', methods=["GET"])
def delete_member():
    """
    Api to delete the member based on the member id
    accepts parameter member_id
    """

    member_id = request.args.get('member_id')
    if member_id:
        try: 
            DataBaseService().delete_one_row('members', member_id, 'member_id')
        except Exception as e:
            abort(500, "Internal server error")
        return json.dumps(True)
    abort(400, "Member id parameter is not sent")

get_members_api = Blueprint('get_members_api', __name__)
@get_members_api.route('/members', methods=["GET"])
def get_members():
    """
        API to get all the member details
        
    """
    try:
        member_details = DataBaseService().select_all('members')
        print(member_details)
        serilized_member_details = convert_row_mapping(member_details)
        return {'data': serilized_member_details}
    except Exception as e:
        abort(500, "Internal server error")





        