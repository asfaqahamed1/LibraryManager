from flask import Blueprint, request, abort 
from decorators.validation_decorators import request_validator
from validators.rent_validator import CloseRentValidator
from database.database_service import DataBaseService
from helpers.utils import convert_row_mapping

rent_api = Blueprint('rent_api', __name__)
@rent_api.route('/rent', methods= ["GET"])
def rent():
    """
    API to register the rent from the member

    """
    book = request.args.get('book_id')
    member = request.args.get('member_id')
    print(book,member)
    if book and member:
        DataBaseService().insert_rent(book, member)
        DataBaseService().mark_book_status(book, False)
        return json.dumps(True)
    abort (401 , "Invalid params")


close_rent_api = Blueprint('close_rent_api', __name__)
@close_rent_api.route('/close_rent', method="POST")
@request_validator(CloseRentValidator)
def close_rent():
    payload = request.get_json()
    try:
        DataBaseService().close_rent(payload['rent_id'], payload['rent_fee'])
        DataBaseService.mark_book_status(payload['book_id'])
        if not payload['paid']:
            DataBaseService.add_current_balance(payload['rent_fee'], payload['member_id'])
    except Exception as e:
        abort (500, "Internal server error")
    return True

rent_details = Blueprint('rent_details', __name__)
@rent_details.route('/rent_details', methods = ["GET"])
def get_rent_details():
    try:
        rental_details = DataBaseService().select_all('rental_details')
        serilized_rental_details = convert_row_mapping(rental_details)
        return {
            'data': serilized_rental_details
        }
    except Exception as e:
        abort (500, "Internal server error")
