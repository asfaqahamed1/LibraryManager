from flask import Blueprint,request, abort
from decorators.validation_decorators import request_validator
from validators.login_validator import LoginValidator
from database.database_service import DataBaseService
from helpers.auth import generate_token_for_user

admin_login_api = Blueprint('admin_login', __name__)

@admin_login_api.route('/login', methods=["POST"])
@request_validator(LoginValidator)
def admin_login():
    request_data = request.get_json()
    admin_details = DataBaseService().get_user(request_data.get('user_name'), request_data.get('password'))
    
    
    if len(admin_details) == 0:
        raise abort(401, "invalid username password")

    token = generate_token_for_user(request_data.get('user_name'))
    return {"data": { "token": token }}