from pydantic import BaseModel

class UpdateBookValidator(BaseModel):
    book_id: int
    title: str
    authors: str
    publisher: str
