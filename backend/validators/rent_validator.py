from pydantic import BaseModel
class CloseRentValidator(BaseModel):
    rent_id: str
    rent_fee: float
    book_id: str
    member_id: str
    paid: bool
