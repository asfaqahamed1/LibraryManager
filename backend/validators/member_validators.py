from pydantic import BaseModel
class AddMemberValidator(BaseModel):
    name: str

class UpdateMemberValidator(BaseModel):
    member_id: int
    name: str
    current_balance: float