from pydantic import BaseModel
class LoginValidator(BaseModel):
    user_name: str
    password: str