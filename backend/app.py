from flask import Flask
import os
from dotenv import load_dotenv
from sqlalchemy import text, create_engine
from helpers.utils import register_blueprints
from apis.admin_login import admin_login_api
from apis.books_api import import_books_api, delete_books_api,update_books_api
from apis.members_api import add_member_api, update_member_api, delete_member_api, get_members_api
from apis.rent_apis import rent_api, rent_details

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

#application definition
app = Flask(__name__)
app.config.from_object(os.environ.get("SERVER_ENVIRONMENT"))

#register application urls
register_blueprints(app, [admin_login_api,import_books_api, delete_books_api, update_books_api,add_member_api, update_member_api,delete_member_api, get_members_api, rent_api,rent_details])

#healtthcheck
@app.route("/healthcheck")
def healthcheck():
    
    return "It is up and running dude"

if __name__ == "__main__":
    app.run(host='localhost', port=3001)

