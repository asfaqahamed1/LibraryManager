import jwt
import os
import datetime

def generate_token_for_user(user_name: str):
    """
    generate a jwt token for the user

    user_name: verified user from the database

    """
    jwt_payload = {"sub": user_name, "exp":datetime.datetime.utcnow() + datetime.timedelta(hours = 6)}
    return jwt.encode(jwt_payload, os.environ.get("JWT_SECRET"), os.environ.get("JWT_ALGO"))