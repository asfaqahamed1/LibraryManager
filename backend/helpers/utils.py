def register_blueprints(app, blueprints: list):
    """
    Function to register the appliation blueprints with prefix

    Args:
        app (_type_):Flask application
        blueprints (_type_): blueprints to registerd
    """

    for blueprint in blueprints:
        app.register_blueprint(blueprint, url_prefix="/library_management/api")


def convert_row_mapping(tableData):
    """
    Function to convert the sql alchemy type RowMapping to noraml json serializable object
    since we need to return it in the response

    Args:
        tableData (_type_): response retrived from the database
    """
    normalized_object = []
    for data in tableData:
        serializable_object = {}
        for key, value in data.items():
            serializable_object[key] = value
        normalized_object.append(serializable_object)
    return normalized_object