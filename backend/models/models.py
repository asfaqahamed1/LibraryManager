"""Model class for the sqlalchemy"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy import Column, String, TIMESTAMP, text,DateTime,func, Boolean, ForeignKey,func, Float
from sqlalchemy.orm import relationship

Base = declarative_base()
metadata = Base.metadata


class AdminModel(Base):
    __tablename__ = "admin"

    admin_id = Column(INTEGER, primary_key=True)
    user_name = Column(String(100), nullable=False)
    password = Column(String(20), nullable=False)
    created_at = Column(
        TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP")
    )


class BooksModel(Base):
    __tablename__ = "books"

    book_id = Column(INTEGER, primary_key=True, unique=True, autoincrement=False)
    title = Column(String(255), nullable=False)
    authors = Column(String(255), nullable=False)
    average_rating = Column(String(10), nullable=False)
    isbn = Column(String(50), unique=True, nullable=False)
    isbn13 = Column(String(50), unique=True, nullable=False)
    language_code = Column(String(10), nullable=False)
    num_page = Column(String(10), nullable=False)
    ratings_count = Column(String(10))
    text_reviews_count = Column(String(10))
    publication_date = Column(String(25))
    publisher = Column(String(255))
    is_available = Column(Boolean ,nullable= False)
    imported_at = Column(TIMESTAMP, server_default=func.now())
    update_at = Column(DateTime, server_default=func.now(),server_onupdate=func.now())
    rentals = relationship("RentalDetails", back_populates="book")

class MembersModel(Base):
    __tablename__ = "members"

    member_id = Column(INTEGER, primary_key=True, autoincrement=True)
    name = Column(String(100),nullable=False)
    #should this be double?
    currrent_balance = Column(Float, default=0.0)
    created_at = Column(DateTime, server_default=func.now())
    update_at = Column(DateTime, server_default=func.now(), server_onupdate=func.now())
    rentals = relationship("MembersModel", back_populates="member")

class RentalDetails(Base):
    __tablename__ = "rental_details"

    rental_details_id = Column(INTEGER, primary_key=True, autoincrement=True)
    member_id = Column(INTEGER, ForeignKey("members.member_id"), nullable=False)
    book_id = Column(INTEGER, ForeignKey("books.book_id"), nullable=False)
    fee = Column(Float, default=0.0)
    rented_at= Column(DateTime, server_default=func.now())
    returned_at= Column(DateTime)
    is_returned = Column(Boolean, nullable=False)
    member = relationship("MembersModel", back_populates="rentals")
    book = relationship("BooksModel", back_populates="rentals")
