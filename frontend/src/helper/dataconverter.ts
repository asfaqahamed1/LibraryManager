
import { RentModel, RentDetailsModel, MembersModel, BooksModel } from './interfaces';

export function manipulateRentDetails(rent_details: RentModel[], member_details: MembersModel[], books_details: BooksModel[]): RentDetailsModel[] {
    const manipulatedRentDetails = rent_details.map((rent_detail) => ({
        ...rent_detail,
        is_returned: Boolean(rent_detail.is_returned), 
        member_name: member_details.find(member => member.member_id === rent_detail.member_id)?.name,
        book_name: books_details.find(book => book.book_id === rent_detail.book_id)?.title
    }))
    return manipulatedRentDetails
}