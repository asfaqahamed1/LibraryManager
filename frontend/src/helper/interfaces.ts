export interface TextFieldProps {
    id: string
    label: string
    modelValue: string | number
    invalid?: boolean
    validationStatus?: "valid" | "invalid" | "not-validated"
    errorMessage?: string | undefined
}

export enum ValidTabPages { '/books', '/members', '/returns', '/transaction' }

export interface NavBarItems {
    name: string,
    route: '/books' | '/members' | '/rents' | '/transaction'
}

export interface AppStateModel {
    books: BooksModel[]
    members: MembersModel[]
    rents: RentDetailsModel[]
}
export interface RentModel {
    rented_at: string 
    rental_details_id: number
    member_id:number
    book_id:string
    fee: number
    returned_at: string
    is_returned: number
}
export interface RentDetailsModel {
    rented_at: string 
    rental_details_id: number
    member_id:number
    book_id:string
    fee: number
    book_name: string | undefined
    member_name: string | undefined
    returned_at: string
    is_returned: boolean
}
export interface MembersModel {
    created_at: string
    currrent_balance: number
    member_id: number
    name: string
    update_at: string
}
export interface BooksModel {
    book_id: string,
    title: string,
    authors: string,
    average_rating: string,
    isbn: string,
    isbn13: string,
    language_code: string,
    num_page: string,
    ratings_count: string,
    text_reviews_count: string,
    publication_date: string,
    publisher: string
}



export interface BookModalProps {
    showmodal: boolean,
    author: string,
    publisher: string,
    title: string,
    book_id: string
}