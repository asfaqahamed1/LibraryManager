const fetchT = (input: RequestInfo, init?: RequestInit) =>
    fetch(input, init).then((res) => {
        if (!res.ok) {
            if (res.status === 430) {
                return res.json();
            }
            throw new Error("Network response was not OK");
        }
        return res.json();
    });

const config = {
    base_url: import.meta.env.VITE_BASE_URL,
    token: undefined
}

const getHeaders = () => {
    const headers = new Headers();
    headers.set("Content-Type", "application/json");
    if (config.token) {
        headers.set("Authorization", `Bearer ${config.token}`);
    } else {
        const localToken = getLocalToken();
        if (localToken) {
            headers.set("Authorization", `Bearer ${localToken}`);
        }
    }
    return headers;
};

const fetchJSON = async (url: string, data: object | undefined) => {
    const headers = getHeaders();
    return fetchT(config.base_url + url, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers,
        body: data ? JSON.stringify(data) : undefined,
    });
};

const getJSON = async (url: string) => {
    const headers = getHeaders();
    return fetchT(config.base_url + url, { headers });
};

const getLocalToken = () => localStorage.getItem('token')

const setLocalToken = (token: string) => localStorage.setItem('token', token)

export const adminLogin = async (userName: string, password: string) => {
    try {
        const response = await fetchJSON('/library_management/api/login', { user_name: userName, password: password })
        setLocalToken(response['data']['token'])
        return true
    }
    catch (e) {
        return false
    }
}

export const importBooksApi = async (title: string | undefined, author: string | undefined, publisher: string | undefined) => {
    try {
        const response = await getJSON(`/library_management/api/import_books?title=${title}&authors=${author}&publishers=${publisher}`)
        return response.data
    }
    catch(e) {
        throw e;
    }
}

export const updateBooksApi = async (book_id: string, title: string, authors: string, publisher: string) => {
    try {
        const response = await fetchJSON(`/library_management/api/update_book`, {book_id, title, authors, publisher})
        return response.data
    }
    catch(e) {
        throw e;
    }
}

export const addMemberApi = async (name: string) => {
    try {
        await fetchJSON('/library_management/api/addmember', { name })
        return true
    }
    catch (e) {
        throw new Error("Add member failed");

    }
}

export const rentBookApi = async (member_id: string , book_id: string) => {
    try {
        await getJSON(`/library_management/api/rent?book_id=${book_id}&member_id=${member_id}`)
        return true
    }
    catch(e){
        return false
    }
}

export const getRentDetails = async() => {
    try{
        const rent_details = await getJSON('/library_management/api/rent_details')
        return rent_details.data
    }
    catch(e){
        throw e
    }
}

export const getMembers = async () => {
    const response = await getJSON('/library_management/api/members')
    return response.data
} 

export const deleteBookApi = async (book_id: string) => {
    const response = await getJSON(`/library_management/api/delete_book?book_id=${book_id}`)
    return response
}