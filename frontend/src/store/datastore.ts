import { defineStore } from "pinia";
import { reactive, toRefs } from "vue";
import { NavBarItems } from "../helper/interfaces"
import { AppStateModel } from "../helper/interfaces"

export const datastore = defineStore("datastore", () => {
    const state: AppStateModel = reactive({
        books: [],
        members: [],
        rents: []
    })
    const stateAsRefs = toRefs(state)

    const navBarItems: NavBarItems[] = [{
        name: "Books",
        route: "/books",
    }, {
        name: "Members",
        route: "/members"
    }, {
        name: "Rents",
        route: "/rents"
    }, {
        name: "Transaction",
        route: "/transaction"
    }
    ]
    return {
        navBarItems,
        ...stateAsRefs,
    }

})