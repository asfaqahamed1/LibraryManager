import { defineStore } from "pinia";
import { datastore } from "./datastore";
import {manipulateRentDetails} from '../helper/dataconverter'
import { adminLogin, importBooksApi, addMemberApi, getMembers, deleteBookApi, updateBooksApi, rentBookApi, getRentDetails } from "../service/apiservices";
import { useRouter } from "vue-router";
import { BooksModel, MembersModel } from "../helper/interfaces";

export const appstore = defineStore("appstore", () => {
    const data = datastore()
    const router = useRouter()

    const filterBooks = (searchString: string) => {
        if (searchString.trim() == "") return data.books
        else {
            const filteredBookList = data.books.filter((value: BooksModel) =>
                value.authors.toLowerCase().includes(searchString) ||
                value.title.toLowerCase().includes(searchString) ||
                value.isbn.toLowerCase().includes(searchString) ||
                value.publisher.toLowerCase().includes(searchString))
            return filteredBookList
        }
    }

    const filterMembers = (searchString: string) => {
        if (searchString.trim() === "") return data.members
        else {
            const filterMemberList = data.members.filter((value: MembersModel) => value.name.toLowerCase().includes(searchString))
            return filterMemberList
        }
    }

    const login = async (userName: string, passWord: string) => {
        const isValidUser = await adminLogin(userName, passWord)
        if (isValidUser) router.replace('/home')
        return isValidUser
    }

    const importBooks = async (title: string | undefined, author: string | undefined, publisher: string | undefined) => {
        const bookDetails = await importBooksApi(title, author, publisher)
        data.books = bookDetails
    }

    const updateBook = async (title: string, author: string, publisher: string, book_id: string) => {
        const bookDetails = await updateBooksApi(book_id, title, author, publisher)
        data.books = bookDetails
    }

    const addMember = async (name: string) => {
        try {
            await addMemberApi(name)
            await getMember()
            return true
        }
        catch {
            return false
        }
    }

    const getMember = async () => {
        data.members = await getMembers()
    }

    const rentBook = async (member_id: string, book_id: string) => {
        const rentStatus = await rentBookApi(member_id, book_id)
        return rentStatus

    }

    const rentDetails = async () => { 
        const rent_details = await getRentDetails()
        data.rents = manipulateRentDetails(rent_details, data.members, data.books)
    }

    const deleteBook = async (bookDetails: any) => {
        const response = await deleteBookApi(bookDetails.data.book_id)
        if (response) await importBooks(undefined, undefined, undefined)
    }


    return {
        login,
        importBooks,
        addMember,
        getMember,
        filterBooks,
        filterMembers,
        deleteBook,
        updateBook,
        rentBook,
        rentDetails,
        data
    }
})