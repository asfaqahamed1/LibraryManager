import { createRouter, createWebHistory } from "vue-router";
import LoginView from "../views/LoginView.vue"
import HomeView from "../views/HomeView.vue"
import RentView from "../views/RentView.vue"
const routes = [{
    path: "/",
    name: "/",
    component: LoginView
},
{
    path: "/home",
    name: "home",
    component: HomeView
},
{
    path:"/rents",
    name: "rents",
    component: RentView
}
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router