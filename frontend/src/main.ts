import { createApp } from 'vue'
import router from "./router/router"
import App from "./App.vue"
import { createPinia } from 'pinia'
import PrimeVue from 'primevue/config'
import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";


const app = createApp(App)
app.use(PrimeVue)
app.use(router);
app.use(createPinia())
app.mount("#app");
